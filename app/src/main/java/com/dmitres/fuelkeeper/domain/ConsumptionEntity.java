package com.dmitres.fuelkeeper.domain;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.nio.channels.Pipe;

@Entity(tableName = "consumptions")
public class ConsumptionEntity {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "consumption_id", typeAffinity = ColumnInfo.INTEGER)
    private int consumptionId;

    @ColumnInfo(name = "date", typeAffinity = ColumnInfo.INTEGER)
    private long date;

    @ColumnInfo(name = "fuel_count", typeAffinity = ColumnInfo.REAL)
    private double fuelCount;

    @ColumnInfo(name = "fuel_price", typeAffinity = ColumnInfo.REAL)
    private double fuelPrice;

    @ColumnInfo(name = "odometer", typeAffinity = ColumnInfo.INTEGER)
    private int odometer;

    @ColumnInfo(name = "note", typeAffinity = ColumnInfo.TEXT)
    private String note;

    @Ignore()
    private double consumption;

    //

    public int getConsumptionId() {
        return consumptionId;
    }

    public void setConsumptionId(int consumptionId) {
        this.consumptionId = consumptionId;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public double getFuelCount() {
        return fuelCount;
    }

    public void setFuelCount(double fuelCount) {
        this.fuelCount = fuelCount;
    }

    public double getFuelPrice() {
        return fuelPrice;
    }

    public void setFuelPrice(double fuelPrice) {
        this.fuelPrice = fuelPrice;
    }

    public double calcPrice() {
        try {
            if (fuelCount != 0.0) {
                return fuelPrice / fuelCount;
            }
        } catch (Exception ignore) {
        }
        return 0;
    }

    public int getOdometer() {
        return odometer;
    }

    public void setOdometer(int odometer) {
        this.odometer = odometer;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public double getConsumption() {
        return consumption;
    }

    public void setConsumption(double consumption) {
        this.consumption = consumption;
    }
}

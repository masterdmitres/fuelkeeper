package com.dmitres.fuelkeeper.domain;

import android.text.Editable;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public interface FormatUtils {

    NumberFormat fmt = new DecimalFormat("#.##");

    default String formatDouble(double value) {
        return fmt.format(value);
    }

    default String formatLong(long value) {
        return fmt.format(value);
    }

    default String formatInteger(int value) {
        return Integer.toString(value);
    }
    
    default String editDouble(double value) {
        return String.valueOf(value);
    }

    default double stringToDouble(Editable editable, double def) {
        try {
            if (editable != null) {
                String value = editable.toString();
                return Double.parseDouble(value);
            }
        } catch (Exception ignore) {
        }
        return def;
    }

    default int stringToInteger(Editable editable, int def) {
        try {
            if (editable != null) {
                String value = editable.toString();
                return Integer.parseInt(value);
            }
        } catch (Exception ignore) {
        }
        return def;
    }

    default String editableToString(Editable text, String def) {
        if (text != null) {
            String value = String.valueOf(text).trim();
            if (value.length() != 0) {
                return value;
            }
        }
        return def;
    }
    

    default String notNull(String value) {
        return value == null ? "" : value;
    }

}

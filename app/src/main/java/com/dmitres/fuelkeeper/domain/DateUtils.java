package com.dmitres.fuelkeeper.domain;

import android.annotation.SuppressLint;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public interface DateUtils {

    @SuppressLint("ConstantLocale")
    DateFormat simple = new SimpleDateFormat("dd MMMM YYYY", Locale.getDefault());

    default  String showDate(long timestamp){
        return simple.format(new Date(timestamp));
    }



}

package com.dmitres.fuelkeeper.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.core.util.Consumer;

import com.dmitres.fuelkeeper.R;
import com.dmitres.fuelkeeper.databinding.ConsumptionEditBinding;
import com.dmitres.fuelkeeper.domain.ConsumptionEntity;
import com.dmitres.fuelkeeper.domain.DateUtils;
import com.dmitres.fuelkeeper.domain.FormatUtils;

public class ConsumptionDialog implements FormatUtils, DateUtils {

    private final Context context;
    private ConsumptionEditBinding binding;

    public ConsumptionDialog(Context context) {
        this.context = context;
    }

    public void show(ConsumptionEntity entity, Consumer<ConsumptionEntity> call) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.consumption_edit, null);

        binding = ConsumptionEditBinding.bind(view);
        binding.txtDate.setText(showDate(entity.getDate()));
        binding.txtFuelValue.setText(editDouble(entity.getFuelCount()));
        binding.txtFuelPrice.setText(editDouble(entity.getFuelPrice()));
        binding.txtOdometer.setText(formatInteger(entity.getOdometer()));
        binding.txtNote.setText(notNull(entity.getNote()));

        AlertDialog dialog = new AlertDialog.Builder(context)
                .setView(view)
                .setPositiveButton(R.string.btn_save, null)
                .setNegativeButton(R.string.btn_close, null)
                .create();

        dialog.show();

        dialog.getButton(DialogInterface.BUTTON_POSITIVE).setOnClickListener(v -> {
            prepare(entity);
            call.accept(entity);
        });


    }


    private void prepare(ConsumptionEntity entity) {

        binding.loFuelValue.setError(null);

        double fuelCount = stringToDouble(binding.txtFuelValue.getText(), -1);
        if (fuelCount < 1 || fuelCount > 100) {
            String msg = context.getString(R.string.err_fuel_value, formatInteger(1), formatInteger(100));
            binding.loFuelValue.setError(msg);
            return;
        }
        entity.setFuelCount(fuelCount);

        double fuelPrice = stringToDouble(binding.txtFuelPrice.getEditableText(), -1);
        entity.setFuelPrice(fuelPrice);

        int odometer = stringToInteger(binding.txtOdometer.getEditableText(), -1);
        entity.setOdometer(odometer);

        String note = editableToString(binding.txtNote.getText(), null);
        entity.setNote(note);
    }


}

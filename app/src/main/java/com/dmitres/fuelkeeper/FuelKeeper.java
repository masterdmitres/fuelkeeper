package com.dmitres.fuelkeeper;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.dmitres.fuelkeeper.db.MainDatabase;

public class FuelKeeper extends Application {

    private static MainDatabase database;

    @Override
    public void onCreate() {
        super.onCreate();
        database = Room.databaseBuilder(getApplicationContext(), MainDatabase.class, "maindb")
                .setJournalMode(RoomDatabase.JournalMode.AUTOMATIC)
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .addCallback(new RoomDatabase.Callback() {
                    @Override
                    public void onCreate(@NonNull SupportSQLiteDatabase db) {
                        super.onCreate(db);
                    }
                }).build();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        if (database != null) {
            database.close();
        }
    }

    public static MainDatabase getDatabase() {
        return database;
    }

}

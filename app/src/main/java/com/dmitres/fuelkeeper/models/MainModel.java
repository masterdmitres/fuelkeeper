package com.dmitres.fuelkeeper.models;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.dmitres.fuelkeeper.FuelKeeper;
import com.dmitres.fuelkeeper.db.Repository;
import com.dmitres.fuelkeeper.domain.ConsumptionEntity;

import java.util.List;

public class MainModel extends ViewModel {

    private final Repository repository;

    private final LiveData<List<ConsumptionEntity>> entities;

    public MainModel() {
        this.repository = FuelKeeper.getDatabase().getRepository();
        entities = repository.findConsumptions();
    }

    //

    public LiveData<List<ConsumptionEntity>> entities() {
        return entities;
    }

    public void saveConsumption(ConsumptionEntity entity) {
        repository.insertConsumption(entity);
    }

    public void update(ConsumptionEntity entity) {
        repository.updateConsumption(entity);
    }
}

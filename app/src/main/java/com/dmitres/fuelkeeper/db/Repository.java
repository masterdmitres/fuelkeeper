package com.dmitres.fuelkeeper.db;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.dmitres.fuelkeeper.domain.ConsumptionEntity;

import java.util.List;

@Dao()
public abstract class Repository {


    //public abstract List<ConsumptionEntity> findConsumptions();

    @Query("SELECT * FROM consumptions")
    public abstract LiveData<List<ConsumptionEntity>> findConsumptions();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract void insertConsumption(ConsumptionEntity entity);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    public abstract void updateConsumption(ConsumptionEntity element);

    @Delete()
    public abstract void deleteConsumption(ConsumptionEntity element);

}

package com.dmitres.fuelkeeper.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.dmitres.fuelkeeper.domain.ConsumptionEntity;

@Database(version = 1, exportSchema = false, entities = ConsumptionEntity.class)
public abstract class MainDatabase extends RoomDatabase {

    public abstract Repository getRepository();

}

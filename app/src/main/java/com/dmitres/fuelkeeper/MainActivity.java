package com.dmitres.fuelkeeper;


import android.content.Context;
import android.icu.util.VersionInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dmitres.fuelkeeper.databinding.ConsumptionElementBinding;
import com.dmitres.fuelkeeper.databinding.MainActivityBinding;
import com.dmitres.fuelkeeper.dialogs.ConsumptionDialog;
import com.dmitres.fuelkeeper.domain.ConsumptionEntity;
import com.dmitres.fuelkeeper.domain.DateUtils;
import com.dmitres.fuelkeeper.domain.FormatUtils;
import com.dmitres.fuelkeeper.models.MainModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private MainModel model;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        MainActivityBinding binding = MainActivityBinding.inflate(layoutInflater);
        setContentView(binding.getRoot());
        model = new ViewModelProvider(this).get(MainModel.class);

        binding.list.setLayoutManager(new LinearLayoutManager(this));
        ConsumptionAdapter adapter;
        binding.list.setAdapter(adapter = new ConsumptionAdapter(this));

        model.entities().observe(this, adapter::update);

        binding.fab.setOnClickListener(v -> {
            ConsumptionEntity entity = new ConsumptionEntity();
            entity.setDate(System.currentTimeMillis());
            new ConsumptionDialog(v.getContext()).show(entity, newentity -> {
                model.saveConsumption(newentity);
            });
        });

    }

    private final View.OnClickListener listener = v -> {
        ConsumptionEntity entity = (ConsumptionEntity) v.getTag();
        new ConsumptionDialog(this).show(entity, newentity -> {
            model.update(newentity);
        });
    };

    public class ConsumptionHolder extends RecyclerView.ViewHolder implements FormatUtils, DateUtils {

        private final ConsumptionElementBinding binding;

        public ConsumptionHolder(@NonNull View view) {
            super(view);
            this.binding = ConsumptionElementBinding.bind(view);
        }

        public void update(ConsumptionEntity entity) {
            binding.getRoot().setTag(entity);
            binding.getRoot().setOnClickListener(listener);
            binding.txtDate.setText(showDate(entity.getDate()));
            binding.txtFuelCount.setText(formatDouble(entity.getFuelCount()));
            binding.txtFuelPrice.setText(formatDouble(entity.getFuelPrice()));
            binding.txtPrice.setText(formatDouble(entity.calcPrice()));
        }
    }

    public class ConsumptionAdapter extends RecyclerView.Adapter<ConsumptionHolder> {

        private final LayoutInflater inflater;
        private ConsumptionEntity[] entities;

        public ConsumptionAdapter(Context context) {
            this.inflater = LayoutInflater.from(context);
            this.entities = new ConsumptionEntity[0];
        }

        @NonNull
        @Override
        public ConsumptionHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = inflater.inflate(R.layout.consumption_element, parent, false);
            return new ConsumptionHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ConsumptionHolder holder, int position) {
            holder.update(entities[position]);
        }

        @Override
        public int getItemCount() {
            return entities.length;
        }

        public void update(List<ConsumptionEntity> entities) {
            this.entities = new ConsumptionEntity[entities.size()];
            entities.toArray(this.entities);
            notifyDataSetChanged();
        }

    }


}
